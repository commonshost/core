# Deployment

## System Dependencies

- [acme.sh](https://acme.sh)
- [s3cmd](http://www.s3tools.org/s3cmd)

## Service Dependencies

- [LetsEncrypt](https://letsencrypt.org) or ACME API-compatible Certificate Authority
- [Auth0](https://www.auth0.com)
- [MongoDB Atlas](https://www.mongodb.com/cloud/atlas) or any MongoDB instance
- [DigitalOcean Spaces](https://www.digitalocean.com/products/object-storage/) or [AWS S3](https://aws.amazon.com/s3/) API-compatible object store
- [PubNub](https://www.pubnub.com)

## Usage

Runs as a command line application.

```
commonshost-core <configuration>
```

Where `<configuration>` is the path to a file containing the configuration options. It can be JavaScript or JSON.

## Configuration

Example of a configuration file using environment variables.

See:
[`./core.conf.example.js`](./core.conf.example.js)

## AWS IAM Policies

### Users

- `commonshost-core-production`
- `commonshost-core-test`
- `commonshost-edge-production`
- `commonshost-edge-test`

### Buckets

- `commonshost-production`
- `commonshost-test`

### Policies

#### Upload or Delete Files

##### core-production

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListAllMyBuckets"
      ],
      "Resource": "arn:aws:s3:::*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetObject",
        "s3:ListBucket",
        "s3:PutObject",
        "s3:PutObjectAcl",
        "s3:DeleteObject"
      ],
      "Resource": [
        "arn:aws:s3:::commonshost-production",
        "arn:aws:s3:::commonshost-production/*"
      ]
    }
  ]
}
```

##### core-test

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListAllMyBuckets"
      ],
      "Resource": "arn:aws:s3:::*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetObject",
        "s3:ListBucket",
        "s3:PutObject",
        "s3:PutObjectAcl",
        "s3:DeleteObject"
      ],
      "Resource": [
        "arn:aws:s3:::commonshost-test",
        "arn:aws:s3:::commonshost-test/*"
      ]
    }
  ]
}
```

#### List and Read Files

##### edge-production

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListAllMyBuckets"
      ],
      "Resource": "arn:aws:s3:::*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListBucket",
        "s3:GetObject"
      ],
      "Resource": [
        "arn:aws:s3:::commonshost-production",
        "arn:aws:s3:::commonshost-production/*"
      ]
    }
  ]
}
```

##### edge-test

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListAllMyBuckets"
      ],
      "Resource": "arn:aws:s3:::*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListBucket",
        "s3:GetObject"
      ],
      "Resource": [
        "arn:aws:s3:::commonshost-test",
        "arn:aws:s3:::commonshost-test/*"
      ]
    }
  ]
}
```
