# Certificates

## `GET /sites{/domain}/certificate`

Scope: `global_read` or `deploy`

Retrieves TLS public certificate with SAN records, CA chain, and private key as PEM strings.

Response body:

```js
{
  modified: '2020-02-20T02:20:02.202',
  domain: 'example.net',
  san: [
    'example.net',
    '*.example.net',
    'a.example.com',
    'b.example.com'
  ],
  ca: ['...', '...'],
  certificate: '...',
  key: '...',
}
```

## `PUT /sites{/domain}/certificate`

*Not yet implemented.*

Issue a certificate with the given Subject Alternative Name (SAN or `subjectAltName`) extension field.

Request body:

```js
{
  san: [
    'example.net',
    '*.example.net',
    'a.example.com',
    'b.example.com'
  ]
}
```
