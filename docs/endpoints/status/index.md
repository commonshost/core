# Status

## `GET /`

Retrieve server health information. The `uptime` value is measured in seconds.

```js
{
  status: 'OK',
  uptime: 120
}
```
