# Files

## `GET /v2/sites{/domain}/files{/directory}/`

*Not yet implemented.*

List all files in a given directory.

## `DELETE /v2/sites{/domain}/files{/file}`

*Not yet implemented.*

Remove a file.

## `PUT /v2/sites{/domain}/files{/file}`

*Not yet implemented.*

Add or replace a file.
