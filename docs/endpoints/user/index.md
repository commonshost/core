# User

## `PATCH /v2/users{/id}`

Update the email address or password of the authenticated user.
