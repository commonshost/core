# Site Information

## `GET /v2/sites{/domain}/info`

*Not yet implemented.*

Get site metadata.

## `PATCH /v2/sites{/domain}/info`

*Not yet implemented.*

Edit site metadata.

## `PUT /v2/sites{/domain}/info`

*Not yet implemented.*

Set site metadata.
