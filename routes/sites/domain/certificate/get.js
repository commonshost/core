const jwtPermissions = require('express-jwt-permissions')
const { checkDomainAccess } = require('../../../../hooks/checkDomainAccess')
const { NotFound } = require('http-errors')

module.exports = async (fastify, options) => {
  fastify.use(
    jwtPermissions({ permissionsProperty: 'scope' })
      .check([['deploy'], ['global_read']])
  )

  fastify.route({
    method: 'GET',
    url: '/sites/:domain/certificate',
    schema: {
      response: {
        200: {
          type: 'object',
          properties: {
            modified: { type: 'string' },
            domain: { type: 'string' },
            san: {
              type: 'array',
              items: { type: 'string' }
            },
            ca: {
              type: 'array',
              items: { type: 'string' }
            },
            cert: { type: 'string' },
            key: { type: 'string' }
          }
        }
      }
    },
    preHandler: [
      checkDomainAccess()
    ],
    handler: async (request, reply) => {
      const { domain } = request.params
      const { db } = fastify.mongo

      const certificate = await db.collection('certificates')
        .findOne({ domain })

      if (certificate) {
        return certificate
      } else {
        throw new NotFound()
      }
    }
  })
}
