const { tmpdir } = require('os')
const { dirname, join } = require('path')
const {
  createReadStream,
  createWriteStream,
  promises: {
    mkdir,
    rename
  }
} = require('fs')
const tmp = require('tmp-promise')
const mime = require('mime/lite')
const compressible = require('compressible')
const { createBrotliCompress: brotli } = require('zlib')
const streamToPromise = require('stream-to-promise')
const { s3cmd } = require('../../../helpers/s3cmd')
const jwtPermissions = require('express-jwt-permissions')
const multer = require('multer')
const bytes = require('bytes')
const { ConfigurationValidator } = require('@commonshost/configuration')
const { BadRequest, Unauthorized, Forbidden } = require('http-errors')
const { lowestCommonAncestor } = require('lowest-common-ancestor')
const { fromAfter } = require('from-after')
const { issueCertificate } = require('../../../helpers/issueCertificate')
const fileFilter = require('../../../helpers/fileFilter')

module.exports = async (fastify, options) => {
  fastify.addContentTypeParser(
    'multipart/form-data',
    (request, done) => done(null, request)
  )

  fastify.use(
    jwtPermissions({ permissionsProperty: 'scope' })
      .check('deploy')
  )

  fastify.use(multer({
    dest: fastify.configuration.upload || tmpdir(),
    preservePath: true,
    fileFilter,
    limits: {
      fileSize: bytes('100MB'),
      files: 100000
    }
  }).array('directory'))

  fastify.route({
    method: 'PUT',
    url: '/sites/:domain',
    schema: {
      response: {
        200: {
          type: 'object',
          properties: {
            type: { type: 'string', value: 'site-deploy' },
            domain: { type: 'string' },
            isNewDomain: { type: 'boolean' },
            hasNewFiles: { type: 'boolean' },
            hasNewConfiguration: { type: 'boolean' }
          }
        }
      }
    },
    handler: async (request, reply) => {
      const { configuration } = fastify
      const { db } = fastify.mongo
      const { domain } = request.params
      const userId = request.req.user.sub
      let hostConfiguration
      let hasNewConfiguration = false

      if (request.req.body && request.req.body.configuration) {
        const total = request.req.body.configuration.length
        const limit = 1e4
        if (total > limit) {
          throw new Forbidden(
            `Configuration (${total} bytes) exceeds limit (${limit} bytes).`
          )
        }
        hostConfiguration = JSON.parse(request.req.body.configuration)
        const validator = new ConfigurationValidator()
        try {
          validator.validate({ hosts: [hostConfiguration] })
        } catch (error) {
          throw new BadRequest(
            'Invalid configuration: Does not comply with the JSON Schema.'
          )
        }
        delete hostConfiguration.domain
        delete hostConfiguration.root
        hasNewConfiguration = true
      }

      const hasNewFiles = Array.isArray(request.req.files) &&
        request.req.files.length > 0

      if (hasNewFiles === true) {
        const limit = 1e6
        const total = request.req.files.reduce((sum, size) => sum + size, 0)
        if (total > limit) {
          throw new Forbidden(
            `Files (${total} bytes) exceed limit (${limit} bytes).`
          )
        }
      }

      {
        const labels = domain.split('.')
        const parents = []
        while (labels.shift() && labels.length > 1) {
          const domain = labels.join('.')
          if (domain === 'commons.host') {
            break
          }
          parents.push(domain)
        }
        if (parents.length) {
          const conflict = await db.collection('hosts').find({
            domain: { $in: parents },
            ownerId: { $ne: userId }
          }, { limit: 1 }).toArray()
          if (conflict.length > 0) {
            throw new Unauthorized(`Unauthorized subdomain: "${domain}"`)
          }
        }
      }

      let isNewDomain = false
      try {
        const result = await db.collection('hosts').updateOne(
          { domain, ownerId: userId },
          {
            $set: { domain, ownerId: userId },
            $currentDate: { modified: true }
          },
          { upsert: true }
        )
        isNewDomain = result.modifiedCount === 0
      } catch (error) {
        const mongoErrorDuplicateKey = 11000
        if (error.code !== mongoErrorDuplicateKey) {
          request.log.error(error)
        }
        throw new Unauthorized(`Unauthorized domain: "${domain}"`)
      }

      if (hasNewConfiguration) {
        await db.collection('configurations').updateOne(
          { domain },
          {
            $set: { domain, configuration: hostConfiguration },
            $currentDate: { modified: true }
          },
          { upsert: true }
        )
      } else {
        try {
          await db.collection('configurations').insertOne({
            domain,
            configuration: {},
            modified: new Date()
          })
        } catch (error) {
        }
      }

      const response = {
        type: 'site-deploy',
        domain,
        isNewDomain,
        hasNewFiles,
        hasNewConfiguration
      }
      reply.send(response)

      issueCertificate(fastify, domain)
        .catch((error) => {
          request.log.error(error.message)
          if (error.stdout) {
            request.log.error(error.stdout)
            request.log.error(error.stdout.toString())
          } else {
            request.log.info('No error.stdout')
          }
          if (error.stderr) {
            request.log.error(error.stderr)
            request.log.error(error.stderr.toString())
          } else {
            request.log.info('No error.stderr')
          }
        })

      if (hasNewFiles === true) {
        const originalnames = new Set(
          request.req.files.map(({ originalname }) => originalname)
        )
        const ancestor = lowestCommonAncestor(...originalnames)

        const stagingDirectory = await tmp.dir({ unsafeCleanup: true })

        try {
          for (const file of request.req.files) {
            const filepath = fromAfter.call(file.originalname, ancestor)
            const destination = join(stagingDirectory.path, 'public', filepath)
            await mkdir(dirname(destination), { recursive: true })
            await rename(file.path, destination)
            if (compressible(mime.getType(destination))) {
              const encodings = [['br', brotli]]
              for (const [extension, encoder] of encodings) {
                if (!originalnames.has(`${file.originalname}.${extension}`)) {
                  const encodedPath = `${destination}.${extension}`
                  const encodedOutput = createWriteStream(encodedPath)
                  createReadStream(destination)
                    .pipe(encoder())
                    .pipe(encodedOutput)
                  await streamToPromise(encodedOutput)
                }
              }
            }
          }

          await s3cmd(
            configuration.s3,
            '--delete-removed',
            'sync',
            join(stagingDirectory.path, 'public') + '/',
            `s3://${configuration.s3.bucket}/sites/${domain}/public/`
          )
        } catch (error) {
          request.log.error(error)
        }

        stagingDirectory.cleanup()
      }

      await fastify.pubnub.publish({
        message: response,
        channel: configuration.pubnub.channels[0]
      })

      request.log.info(`Deployed domain: ${domain}`)
    }
  })
}
