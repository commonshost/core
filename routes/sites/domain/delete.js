const jwtPermissions = require('express-jwt-permissions')
const { checkDomainAccess } = require('../../../hooks/checkDomainAccess')
const { s3cmd } = require('../../../helpers/s3cmd')

module.exports = async (fastify, options) => {
  fastify.use(
    jwtPermissions({ permissionsProperty: 'scope' })
      .check('deploy')
  )

  fastify.route({
    method: 'DELETE',
    url: '/sites/:domain',
    schema: {},
    preHandler: [
      checkDomainAccess()
    ],
    handler: async (request, reply) => {
      const { configuration } = fastify
      const { domain } = request.params
      const { db } = fastify.mongo

      await Promise.all([
        db.collection('configurations').deleteOne({ domain }),
        db.collection('hosts').deleteOne({ domain }),
        db.collection('certificates').deleteOne({ domain }),
        s3cmd(
          configuration.s3,
          '--recursive',
          'del',
          `s3://${configuration.s3.bucket}/sites/${domain}/`
        )
      ])

      await fastify.pubnub.publish({
        channel: configuration.pubnub.channels[0],
        message: { type: 'site-delete', domain }
      })

      reply.send()
    }
  })
}
