const jwtPermissions = require('express-jwt-permissions')
const { Gone } = require('http-errors')
const { domainToASCII } = require('url')
const isDomainName = require('is-domain-name')
const isValidDomain = require('is-valid-domain')

module.exports = async (fastify, options) => {
  fastify.use(
    jwtPermissions({ permissionsProperty: 'scope' })
      .check('deploy')
  )

  fastify.route({
    method: 'GET',
    url: '/domains/available',
    schema: {
      querystring: {
        domain: { type: 'string' }
      }
    },
    handler: async (request, reply) => {
      const { db } = fastify.mongo
      const domain = domainToASCII(request.query.domain).toLowerCase()

      if (!isDomainName(domain) || !isValidDomain(domain)) {
        throw new Gone('Domain unavailable')
      }

      const conflict = await db.collection('hosts').findOne({ domain })
      if (conflict) {
        throw new Gone('Domain unavailable')
      }

      reply.send()
    }
  })
}
