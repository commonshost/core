const jwtPermissions = require('express-jwt-permissions')
const { constellix } = require('../../../../helpers/constellix')
const { isIPv4 } = require('net')
const { BadRequest, Forbidden } = require('http-errors')

const serverIdRegex = /^(?<country>[a-z]{2})-(?<iata>[a-z]{3})-(?<index>\d+)$/

let domainId
async function getDomainId (domain, options) {
  if (domainId) return domainId
  const response = await constellix('domains', options)
  const domains = await response.json()
  for (const { name, id } of domains) {
    if (name === domain) {
      return id
    }
  }
  throw new Error(`Domain not found: ${domain}`)
}

const recordIds = new Map()
async function getRecordId (subdomain, domainId, options) {
  if (recordIds.has(subdomain)) {
    return recordIds.get(subdomain)
  }
  const endpoint = `domains/${domainId}/records/A`
  const response = await constellix(endpoint, options)
  const records = await response.json()
  for (const record of records) {
    if (record.name === subdomain) {
      recordIds.set(subdomain, record.id)
      return record.id
    }
  }
  throw new Error(`Record not found: ${subdomain}`)
}

module.exports = async (fastify, options) => {
  fastify.use(
    jwtPermissions({ permissionsProperty: 'scope' })
      .check('ddns')
  )

  fastify.route({
    method: 'PUT',
    url: '/servers/:id/ddns',
    schema: {
      body: {
        type: 'object',
        properties: {
          ipv4: { type: 'string' }
        }
      }
    },
    handler: async (request, reply) => {
      const { body: { ipv4: ip }, params: { id: serverId } } = request
      if (!serverIdRegex.test(serverId)) {
        throw new BadRequest('Invalid server ID')
      }
      if (!isIPv4(ip)) {
        throw new BadRequest('Source address is not IPv4')
      }
      const { db } = fastify.mongo
      const server = await db.collection('servers')
        .findOne({ serverId, allowDdns: true })
      if (!server || server.allowDdns === false) {
        throw new Forbidden('DDNS is not allowed for this server')
      }
      if (server.ipv4 === ip) {
        return { ipv4: ip }
      } else {
        fastify.log.info({ msg: 'ddns-updated', ipv4: ip, server: serverId })
      }
      const apiOptions = fastify.configuration.constellix
      const domainId = await getDomainId(apiOptions.domain, apiOptions)
      const recordId = await getRecordId(serverId, domainId, apiOptions)
      const endpoint = `domains/${domainId}/records/A/${recordId}`
      const method = 'PUT'
      const body = {
        recordOption: 'roundRobin',
        name: serverId,
        ttl: 60,
        roundRobin: [{
          disableFlag: false,
          value: ip
        }]
      }
      const options = {
        body: JSON.stringify(body),
        method,
        ...apiOptions
      }
      constellix(endpoint, options)
      await db.collection('servers').updateOne(
        { serverId, allowDdns: true },
        {
          $set: { ipv4: ip },
          $currentDate: { modified: true }
        },
        { upsert: true }
      )
      return { ipv4: ip }
    }
  })
}
