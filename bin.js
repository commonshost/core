#!/usr/bin/env node

const { join, resolve } = require('path')
const server = require('./server')

if (process.argv[2] === undefined) {
  console.error(
    'Specify the path to a configuration file.\n' +
    `See: ${join(__dirname, 'core.conf.example.js')}`
  )
  process.exit(1)
}

const configuration = require(resolve(process.cwd(), process.argv[2]))
server(configuration)
  .then((fastify) => {
    const { port, host } = configuration
    fastify.listen(port, host, (error) => {
      if (error) {
        console.error(error)
        process.exit(1)
      }

      let notify
      try {
        notify = require('sd-notify')
      } catch (error) {
        fastify.log.warn('Systemd notifications and heartbeat are unavailable')
      }
      if (notify) {
        const watchdogInterval = notify.watchdogInterval()
        if (watchdogInterval > 0) {
          const interval = Math.max(500, Math.floor(watchdogInterval / 2))
          notify.startWatchdogMode(interval)
        }
        notify.ready()
      }
    })
  })
