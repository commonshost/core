const fetch = require('node-fetch')
const https = require('https')
const http = require('http')

module.exports.fetch = (url, options = {}) => {
  const { Agent } = url.startsWith('https:') ? https : http
  if (options.agent === undefined) {
    options.agent = new Agent({
      rejectUnauthorized: false,
      ecdhCurve: 'P-384:P-256'
    })
  }
  return fetch(url, options)
}
