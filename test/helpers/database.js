const test = require('blue-tape')
const { MongoClient } = require('mongodb')

const mongo = {
  db: undefined
}

module.exports = (configuration) => {
  test('Connecting database', async (t) => {
    if (mongo.db === undefined) {
      const { url, database } = configuration.mongodb
      const client = await MongoClient.connect(url, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        promoteBuffers: true
      })
      mongo.db = client.db(database)
      const halt = async () => {
        await client.close()
        mongo.db = undefined
      }
      test.onFinish(halt)
      test.onFailure(halt)
    }
  })
  return mongo
}
