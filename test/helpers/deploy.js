const { fetch } = require('./fetch')
const FormData = require('form-data')

module.exports.deploy = function ({
  accessToken,
  domain,
  files,
  port
}) {
  const form = new FormData()

  form.append(
    'configuration',
    JSON.stringify({ domain }),
    { contentType: 'application/json' }
  )

  for (const { data, filepath } of files) {
    form.append('directory', data, { filepath })
  }

  const origin = `https://localhost:${port}`
  const url = `${origin}/v2/sites/${domain}`
  const response = fetch(url, {
    method: 'PUT',
    headers: { authorization: `Bearer ${accessToken}` },
    body: form
  })

  return response
}
