const test = require('blue-tape')
const configuration = require('../core.conf.example.js')
const {
  getEdgeAccessToken,
  getUserAccessToken
} = require('./helpers/credentials')
const { fetch } = require('./helpers/fetch')

const mongo = require('./helpers/database')(configuration)
const server = require('..')

let fastify
test('Start server', async (t) => {
  fastify = await server(configuration)
  await fastify.listen(configuration.port, configuration.host)
})

const fixture = {
  modified: new Date(),
  domain: 'example.com',
  san: ['www.example.com', 'example.net'],
  ca: [
    Buffer.from('Intermediate CA'),
    Buffer.from('Root CA')
  ],
  key: Buffer.from('Private key'),
  cert: Buffer.from('Public certificate')
}

const expected = {
  modified: fixture.modified.toISOString(),
  domain: fixture.domain,
  san: fixture.san,
  ca: fixture.ca.map((buffer) => buffer.toString()),
  key: fixture.key.toString(),
  cert: fixture.cert.toString()
}

test('Setup database fixtures', async (t) => {
  await mongo.db.collection('certificates').deleteMany()
  await mongo.db.collection('certificates').insertMany([fixture])
})

test('Retrieve site configuration as user', async (t) => {
  const accessToken = await getUserAccessToken()
  const origin = `https://localhost:${configuration.port}`
  const url = `${origin}/v2/sites/${fixture.domain}/certificate`
  const response = await fetch(url, {
    method: 'GET',
    headers: { authorization: `Bearer ${accessToken}` }
  })
  t.true(response.ok)
  const actual = await response.json()
  t.deepEqual(actual, expected)
})

test('Retrieve site configuration as edge', async (t) => {
  const accessToken = await getEdgeAccessToken()
  const origin = `https://localhost:${configuration.port}`
  const url = `${origin}/v2/sites/${fixture.domain}/certificate`
  const response = await fetch(url, {
    method: 'GET',
    headers: { authorization: `Bearer ${accessToken}` }
  })
  t.true(response.ok)
  const actual = await response.json()
  t.deepEqual(actual, expected)
})

test('Retrieve unknown site configuration', async (t) => {
  const accessToken = await getEdgeAccessToken()
  const origin = `https://localhost:${configuration.port}`
  const given = 'unknown-domain.example.net'
  const url = `${origin}/v2/sites/${given}/certificate`
  const response = await fetch(url, {
    method: 'GET',
    headers: { authorization: `Bearer ${accessToken}` }
  })
  t.is(response.status, 404)
  const actual = await response.json()
  t.deepEqual(actual, {
    statusCode: 404,
    error: 'Not Found',
    message: 'Not Found'
  })
})

test('Stop server', (t) => fastify.close())
