const test = require('blue-tape')
const configuration = require('../core.conf.example.js')
const {
  getEdgeAccessToken,
  getUserAccessToken
} = require('./helpers/credentials')
const { fetch } = require('./helpers/fetch')

const mongo = require('./helpers/database')(configuration)
const server = require('..')

const timeAfter = new Date()
const timeFresh = new Date(new Date().setUTCMinutes(-1))
const timeBetween = new Date(new Date().setUTCMinutes(-10))
const timeStale = new Date(new Date().setUTCMinutes(-30))
const timeBefore = new Date(new Date().setUTCMinutes(-40))

const fixtureFresh = {
  modified: timeFresh,
  domain: 'fresh.example.com',
  configuration: { is: 'fresh' }
}
const fixtureStale = {
  modified: timeStale,
  domain: 'stale.example.com',
  configuration: { is: 'stale' }
}

const expectedFresh = Object.assign({}, fixtureFresh, {
  modified: fixtureFresh.modified.toISOString()
})
const expectedStale = Object.assign({}, fixtureStale, {
  modified: fixtureStale.modified.toISOString()
})

const url = `https://localhost:${configuration.port}/v2/configurations`

let fastify
test('Start server', async (t) => {
  fastify = await server(configuration)
  await fastify.listen(configuration.port, configuration.host)
})

test('Setup database fixtures', async (t) => {
  await mongo.db.collection('configurations').deleteMany()
  await mongo.db.collection('configurations').insertMany([
    fixtureFresh,
    fixtureStale
  ])
})

test('List all configurations', async (t) => {
  const accessToken = await getEdgeAccessToken()
  const response = await fetch(url, {
    headers: {
      authorization: `Bearer ${accessToken}`,
      'if-modified-since': timeBetween.toString()
    }
  })
  t.is(response.status, 200)
  t.is(response.headers.get('last-modified'), timeFresh.toString())
  const json = await response.json()
  t.ok(Array.isArray(json))
  t.deepEquals(json, [expectedFresh])
})

test('Only new configurations', async (t) => {
  const accessToken = await getEdgeAccessToken()
  const response = await fetch(url, {
    headers: {
      authorization: `Bearer ${accessToken}`,
      'if-modified-since': timeAfter.toString()
    }
  })
  t.is(response.status, 304)
  t.is(response.headers.has('last-modified'), false)
  const text = await response.text()
  t.is(text, '')
})

test('All configurations', async (t) => {
  const accessToken = await getEdgeAccessToken()
  const response = await fetch(url, {
    headers: {
      authorization: `Bearer ${accessToken}`,
      'if-modified-since': timeBefore.toString()
    }
  })
  t.is(response.status, 200)
  t.is(response.headers.get('last-modified'), timeFresh.toString())
  const json = await response.json()
  t.deepEquals(json, [expectedFresh, expectedStale])
})

test('Without conditional header lists everything', async (t) => {
  const accessToken = await getEdgeAccessToken()
  const response = await fetch(url, {
    headers: {
      authorization: `Bearer ${accessToken}`
    }
  })
  t.is(response.status, 200)
  t.is(response.headers.get('last-modified'), timeFresh.toString())
  const json = await response.json()
  t.deepEquals(json, [expectedFresh, expectedStale])
})

test('Require edge service access token', async (t) => {
  const accessToken = await getUserAccessToken()
  const response = await fetch(url, {
    headers: {
      authorization: `Bearer ${accessToken}`
    }
  })
  t.is(response.status, 403)
  t.is(response.headers.has('last-modified'), false)
})

test('Stop server', (t) => fastify.close())
