const test = require('blue-tape')
const configuration = require('../core.conf.example.js')
const { getUserAccessToken, getUserId } = require('./helpers/credentials')
const { fetch } = require('./helpers/fetch')

const mongo = require('./helpers/database')(configuration)
const server = require('..')

let fastify
test('Start server', async (t) => {
  fastify = await server(configuration)
  await fastify.listen(configuration.port, configuration.host)
})

const fixture = {}

test('Setup database fixtures', async (t) => {
  fixture.domain = 'example.com'
  fixture.userId = await getUserId()
  fixture.modified = new Date()
  fixture.configuration = {}

  await mongo.db.collection('hosts').deleteMany()
  await mongo.db.collection('hosts').insertMany([{
    modified: fixture.modified,
    domain: fixture.domain,
    ownerId: fixture.userId
  }])
})

test('List all sites owned by user', async (t) => {
  const accessToken = await getUserAccessToken()
  const origin = `https://localhost:${configuration.port}`
  const url = `${origin}/v2/sites`
  const response = await fetch(url, {
    method: 'GET',
    headers: { authorization: `Bearer ${accessToken}` }
  })
  t.true(response.ok)
  const sites = await response.json()
  const expected = [{
    domain: fixture.domain,
    modified: fixture.modified.toISOString(),
    ownerId: fixture.userId
  }]
  t.deepEqual(sites, expected)
})

test('Stop server', (t) => fastify.close())
