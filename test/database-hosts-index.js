const test = require('blue-tape')
const configuration = require('../core.conf.example.js')

const mongo = require('./helpers/database')(configuration)
const server = require('..')

let fastify
test('Start server', async (t) => {
  fastify = await server(configuration)
  await fastify.listen(configuration.port, configuration.host)
})

test('Enforce unique index on domain', async (t) => {
  const domain = 'example.com'
  const collections = [
    'hosts',
    'configurations',
    'certificates'
  ]
  for (const collection of collections) {
    await mongo.db.collection(collection).deleteMany()
    await mongo.db.collection(collection).insertOne({ domain })
    await t.shouldFail(
      mongo.db.collection(collection).insertOne({ domain })
    )
  }
})

test('Stop server', (t) => fastify.close())
