const test = require('blue-tape')
const configuration = require('../core.conf.example.js')
const fetch = require('node-fetch')
const {
  promises: {
    writeFile,
    mkdir,
    rmdir
  }
} = require('fs')
const { join } = require('path')

const server = require('..')

let fastify
test('Start server', async (t) => {
  fastify = await server(configuration)
  await fastify.listen(configuration.port, configuration.host)
})

test('ACME webroot challenge', async (t) => {
  const prefix = '/.well-known/acme-challenge/'
  const challenges = join(configuration.acme.webroot, prefix)
  const challenge = 'test'
  await mkdir(challenges, { recursive: true })
  await writeFile(join(challenges, challenge), 'hello world')

  const origin = `http://localhost:${configuration.acme.port}`
  const url = `${origin}${prefix}${challenge}`
  const response = await fetch(url)
  t.is(response.status, 200)
  const text = await response.text()
  t.is(text, 'hello world')

  await rmdir(join(challenges, challenge), { recursive: true })
})

test('ACME file not found', async (t) => {
  const prefix = '/.well-known/acme-challenge/'
  const challenges = join(configuration.acme.webroot, prefix)
  const challenge = 'does-not-exist'
  await mkdir(challenges, { recursive: true })

  const origin = `http://localhost:${configuration.acme.port}`
  const url = `${origin}${prefix}${challenge}`
  const response = await fetch(url)
  t.is(response.status, 404)
  const text = await response.text()
  t.is(text, 'Not Found')

  await rmdir(join(challenges, challenge), { recursive: true })
})

test('Stop server', (t) => fastify.close())
