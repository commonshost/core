const test = require('blue-tape')
const server = require('..')
const configuration = require('../core.conf.example.js')
const fetch = require('node-fetch')

const logs = []
const log = (type) => (message) => {
  const { req, res, err } = message
  console.log(type, { req: !!req, res: !!res, err: !!err })
  logs.push([type, message])
}

const logger = {
  info: log('info'),
  error: log('error'),
  debug: log('debug'),
  fatal: log('fatal'),
  warn: log('warn'),
  trace: log('trace'),
  child: () => logger
}

let fastify
test('Start server', async (t) => {
  fastify = await server({
    ...configuration,
    acme: { ...configuration.acme, logger }
  })
  await fastify.listen(configuration.port, configuration.host)
})

test('Avoid log spam from static 404 errors by bots', async (t) => {
  while (logs.length > 0) logs.pop()
  const origin = `http://localhost:${configuration.acme.port}`
  const target = '/wp-content/plugins/portable-phpmyadmin/wp-pma-mod/index.php'
  const url = `${origin}${target}`
  const response = await fetch(url)
  t.is(response.status, 404)
  const text = await response.text()
  t.is(text, 'Not Found')
  t.isNot(logs.length, 0)
  t.ok(logs.every(([type, { err }]) => err === undefined))
})

test('Stop server', (t) => fastify.close())
