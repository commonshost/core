const test = require('blue-tape')
const configuration = require('../core.conf.example.js')
const { domainToASCII: toASCII } = require('url')
const { getUserAccessToken, getUserId } = require('./helpers/credentials')
const { fetch } = require('./helpers/fetch')

const mongo = require('./helpers/database')(configuration)
const server = require('..')

let fastify
test('Start server', async (t) => {
  fastify = await server(configuration)
  await fastify.listen(configuration.port, configuration.host)
})

test('Setup database fixtures', async (t) => {
  const userId = await getUserId()
  const modified = new Date()

  await mongo.db.collection('hosts').deleteMany()
  await mongo.db.collection('hosts').insertMany([
    {
      domain: 'example.com',
      modified,
      ownerId: userId
    },
    {
      domain: toASCII('💩.example.com'),
      modified,
      ownerId: userId
    }
  ])
})

test('Check available domain', async (t) => {
  const accessToken = await getUserAccessToken()
  const given = 'available.example.com'
  const origin = `https://localhost:${configuration.port}`
  const url = `${origin}/v2/domains/available?domain=${given}`
  const response = await fetch(url, {
    method: 'GET',
    headers: { authorization: `Bearer ${accessToken}` }
  })
  t.is(response.status, 200)
})

test('Check unavailable domain', async (t) => {
  const accessToken = await getUserAccessToken()
  const given = 'example.com'
  const origin = `https://localhost:${configuration.port}`
  const url = `${origin}/v2/domains/available?domain=${given}`
  const response = await fetch(url, {
    method: 'GET',
    headers: { authorization: `Bearer ${accessToken}` }
  })
  t.is(response.status, 410)
  t.deepEqual(
    await response.json(),
    { error: 'Gone', message: 'Domain unavailable', statusCode: 410 }
  )
})

test('Check unavailable IDN domain', async (t) => {
  const accessToken = await getUserAccessToken()
  const given = encodeURIComponent('💩.example.com')
  const origin = `https://localhost:${configuration.port}`
  const url = `${origin}/v2/domains/available?domain=${given}`
  const response = await fetch(url, {
    method: 'GET',
    headers: { authorization: `Bearer ${accessToken}` }
  })
  t.is(response.status, 410)
  t.deepEqual(
    await response.json(),
    { error: 'Gone', message: 'Domain unavailable', statusCode: 410 }
  )
})

test('Check invalid domain', async (t) => {
  const accessToken = await getUserAccessToken()
  const given = 'localhost'
  const origin = `https://localhost:${configuration.port}`
  const url = `${origin}/v2/domains/available?domain=${given}`
  const response = await fetch(url, {
    method: 'GET',
    headers: { authorization: `Bearer ${accessToken}` }
  })
  t.is(response.status, 410)
  t.deepEqual(
    await response.json(),
    { error: 'Gone', message: 'Domain unavailable', statusCode: 410 }
  )
})

test('Stop server', (t) => fastify.close())
