const { domainToASCII } = require('url')
const isDomainName = require('is-domain-name')
const isValidDomain = require('is-valid-domain')
const { BadRequest } = require('http-errors')

module.exports = async (request, reply) => {
  if (request.params && ('domain' in request.params)) {
    request.params.domain = domainToASCII(request.params.domain).toLowerCase()
    if (!isDomainName(request.params.domain) || !isValidDomain(request.params.domain)) {
      throw new BadRequest('Invalid domain name')
    }
  }
}
