const { Forbidden } = require('http-errors')

module.exports.checkDomainAccess = () => async function (request, reply) {
  if (request.req.user.scope.split(' ').includes('global_read')) return

  const fastify = this
  const userId = request.req.user.sub
  const { domain } = request.params
  const { db } = fastify.mongo
  const conflictingHost = await db.collection('hosts')
    .findOne({ domain, ownerId: { $ne: userId } })
  if (conflictingHost) {
    throw new Forbidden(`Not the owner of "${domain}"`)
  }
}
