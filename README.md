# @commonshost/core

API to manage CDN backend services.

The core API:

- receives deployments and other input from clients via its API endpoints,
- uploads files to the object store (S3 or DigitalOcean SPaces),
- stores data in MongoDB (Atlas or self-hosted),
- issues certificates using the ACME protocol (LetsEncrypt),
- publishes notifications to the data stream network (PubNub),
- checks authorisation tokens (Auth0).
