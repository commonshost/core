module.exports.defaultHost = {
  domain: '',
  root: '',
  directories: {
    trailingSlash: 'always'
  },
  fallback: {},
  accessControl: {
    allowOrigin: '*'
  },
  headers: [],
  redirects: [],
  manifest: []
}
