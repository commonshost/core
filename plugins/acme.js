const { join } = require('path')
const { mkdir } = require('fs').promises

const prefix = '/.well-known/acme-challenge/'

module.exports = async (fastify, options) => {
  const logger = 'logger' in options ? options.logger : true
  const server = require('fastify')({ logger })
  server.setNotFoundHandler((request, reply) => {
    reply.code(404).send('Not Found')
  })
  const root = join(options.webroot, prefix)
  await mkdir(root, { recursive: true })
  server.register(require('../routes/get'))
  server.register(require('fastify-static'), { root, prefix })
  fastify.addHook('onClose', () => server.close())
  await server.listen(options.port, options.host)
}

module.exports[Symbol.for('skip-override')] = true
