const adjectives = require('@commonshost/word-lists/adjectives-en.json')
const animals = require('@commonshost/word-lists/animals-en.json')
const superb = require('@commonshost/word-lists/superb-en.json')

function pickOne (list) {
  const index = Math.floor(Math.random() * list.length)
  return list[index]
}

function randomSequence () {
  const prefix = pickOne(superb)
  const adjective = pickOne(adjectives)
  const animal = pickOne(animals)
  return `${prefix}-${adjective}-${animal}`
}

module.exports = randomSequence
