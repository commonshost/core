const { resolve } = require('path')

module.exports = {
  // Top level domain for automatically generated subdomains
  wildcard: process.env.COMMONSHOST_CORE_WILDCARD,

  // Directory where uploaded files will be stored before processing
  upload: process.env.COMMONSHOST_CORE_UPLOAD,

  // Constellix DNS
  constellix: {
    domain: process.env.COMMONSHOST_CORE_CONSTELLIX_DOMAIN,
    origin: process.env.COMMONSHOST_CORE_CONSTELLIX_ORIGIN,
    apiKey: process.env.COMMONSHOST_CORE_CONSTELLIX_API_KEY,
    secretKey: process.env.COMMONSHOST_CORE_CONSTELLIX_SECRET_KEY
  },

  // Object Storage
  s3: {
    s3cmd: process.env.COMMONSHOST_CORE_S3_S3CMD,
    region: process.env.COMMONSHOST_CORE_S3_REGION,
    endpoint: process.env.COMMONSHOST_CORE_S3_ENDPOINT,
    bucket: process.env.COMMONSHOST_CORE_S3_BUCKET,
    accessKeyId: process.env.COMMONSHOST_CORE_S3_ACCESS_KEY_ID,
    secretAccessKey: process.env.COMMONSHOST_CORE_S3_SECRET_ACCESS_KEY
  },

  // Database
  mongodb: {
    database: process.env.COMMONSHOST_CORE_MONGODB_DATABASE,
    url: process.env.COMMONSHOST_CORE_MONGODB_URL
  },

  // Data Stream
  pubnub: {
    uuid: process.env.COMMONSHOST_CORE_PUBNUB_UUID,
    channels: process.env.COMMONSHOST_CORE_PUBNUB_CHANNELS.split(' '),
    publishKey: process.env.COMMONSHOST_CORE_PUBNUB_PUBLISH_KEY,
    subscribeKey: process.env.COMMONSHOST_CORE_PUBNUB_SUBSCRIBE_KEY
  },

  // Authentication Tokens
  jwt: {
    jwksUri: process.env.COMMONSHOST_CORE_JWT_JWKSURI,
    audience: process.env.COMMONSHOST_CORE_JWT_AUDIENCE,
    issuer: process.env.COMMONSHOST_CORE_JWT_ISSUER
  },

  // Auth0 Management API
  // https://auth0.com/docs/api/management/v2/tokens#1-get-a-token
  auth0: {
    issuer: process.env.COMMONSHOST_CORE_JWT_ISSUER,
    audience: process.env.COMMONSHOST_CORE_AUTH0_AUDIENCE,
    clientId: process.env.COMMONSHOST_CORE_AUTH0_CLIENT_ID,
    clientSecret: process.env.COMMONSHOST_CORE_AUTH0_CLIENT_SECRET
  },

  // Address to accept incoming HTTPS connections
  port: process.env.COMMONSHOST_CORE_PORT,
  host: process.env.COMMONSHOST_CORE_HOST,

  // TLS credentials
  crypto: {
    key: process.env.COMMONSHOST_CORE_CRYPTO_KEY,
    cert: process.env.COMMONSHOST_CORE_CRYPTO_CERT,
    ca: [process.env.COMMONSHOST_CORE_CRYPTO_CA]
  },

  // LetsEncrypt
  acme: {
    acmesh: process.env.COMMONSHOST_CORE_ACME_ACMESH,
    home: resolve(process.cwd(), process.env.COMMONSHOST_CORE_ACME_HOME),
    webroot: resolve(process.cwd(), process.env.COMMONSHOST_CORE_ACME_WEBROOT),
    port: process.env.COMMONSHOST_CORE_ACME_PORT,
    host: process.env.COMMONSHOST_CORE_ACME_HOST,
    wildcards: process.env.COMMONSHOST_CORE_ACME_WILDCARDS.split(' ')
  }
}
